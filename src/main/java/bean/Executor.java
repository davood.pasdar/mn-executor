package bean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Execution;
import io.lettuce.core.api.StatefulRedisConnection;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ThreadLocalRandom;

@Singleton
public class Executor {
    private static final Logger LOG = LoggerFactory.getLogger(Executor.class);
    private final StatefulRedisConnection<String, String> redisConnection;
    @Inject
    public Executor(StatefulRedisConnection<String, String> redisConnection) {
        this.redisConnection = redisConnection;
    }

    public Optional<Execution> getExecution(String configId) {
        final String result = redisConnection.sync().get(configId);
        try {
            if(Objects.isNull(result)) return Optional.empty();
            return Optional.ofNullable(new ObjectMapper().readValue(result, Execution.class));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }

    public Optional<Execution> createExecution(String configId) {
        return getExecution(configId)
                .or(() -> {
                    final Execution execution = new Execution()
                            .configId(configId);
                    redisConnection.sync()
                            .set(configId, execution.toJson());
                    doWork(configId).thenRun(() -> cancelExecution(configId));
                    return Optional.of(execution);
                });
    }

    public void cancelExecution(String configId) {
        redisConnection.sync().set(configId, null);
    }


    public CompletionStage<Void> doWork(String configId) {
        return CompletableFuture.runAsync(() -> {
            var workLimit = ThreadLocalRandom.current().nextInt(10, 100 + 1);

            for(int i=0;i<=workLimit;i++) {
                try {
                    if(getExecution(configId).isEmpty())
                        return;
//                    esConnection.indexAsync(new IndexRequest()
//                                    .index(configId)
//                                    .source(Map.of("somename", "somevalue", "anothername", "anothervalue")),
//                            RequestOptions.DEFAULT, new ActionListener<IndexResponse>() {
//                                @Override
//                                public void onResponse(IndexResponse indexResponse) {
//
//                                }
//
//                                @Override
//                                public void onFailure(Exception e) {
//
//                                }
//                            });
                    Thread.sleep(1000);
                    getExecution(configId).ifPresent(execution -> {
                        execution.lastUpdated(new Date());
                        redisConnection.sync()
                                .set(configId, execution.toJson());
                    });
                    LOG.info("Running {}...", configId);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


}
