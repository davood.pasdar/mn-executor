package controller;

import bean.Executor;
import entity.Execution;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.http.HttpStatus;

import javax.inject.Inject;
import java.util.Optional;

@Controller("/")
public class MainController {

    private final Executor executor;
    @Inject
    public MainController(Executor executor) {
        this.executor = executor;
    }

    @Get("/{configId}/active-execution")
    public HttpResponse<Execution> getActiveExecution(@PathVariable String configId) {
        return executor.getExecution(configId)
        .map(HttpResponse::ok)
        .orElse(HttpResponse.notFound());
    }

    @Post("/{configId}/active-execution")
    public HttpResponse<Execution> createExecution(@PathVariable String configId) {
        final Optional<Execution> execution = executor.getExecution(configId);
        if(execution.isPresent()) {
            if(execution.get().isExpired())
                executor.cancelExecution(configId);
            else
                return HttpResponse.notModified();
        }

        return executor.createExecution(configId)
                .map(HttpResponse::ok)
                .orElse(HttpResponse.notModified());
    }

    @Delete("/{configId}/active-execution")
    public HttpStatus cancelExecution(@PathVariable String configId) {
        executor.cancelExecution(configId);
        return HttpStatus.OK;
    }
}
