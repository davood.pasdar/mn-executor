package entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data @Accessors(fluent = true)
public class Execution {
    private @JsonProperty String configId;
    private @JsonProperty Date executionStart = new Date();
    private @JsonProperty Date lastUpdated = new Date();
    private @JsonProperty boolean expired = false;

    public String toJson() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "{}";
        }
    }

    public boolean isExpired() {
        return (new Date().getTime() - lastUpdated.getTime()) > 10000;
    }
}
