The following sample project tries to test different aspects of Micronaut framework by building 
a simple execution framework. The following features are/should be supported:

* Caching for execution information, regardless of which application instance to talk to
* Get execution logic to make sure the executor is still active
* Stateless application that can be simply load balanced and killed in Kubernetes
* Instrumentation has to be sent to influxdb
* GraalVM native image
